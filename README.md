# How to setup a realm with SWITCH edu-ID as an IDP on the keycloak server of BiomedIT

[[_TOC_]]

## Prerequisites

This tutorial assumes that:

- You have access to your new realm on the keycloak server of the DCC (https://keycloak-test.dcc.sib.swiss or https://login.biomedit.ch). If that's not the case simply send an email to: biomedit@sib.swiss.
- You have a SWITCH edu-ID account (https://www.switch.ch/edu-id/)
- You have OpenSSL (https://www.openssl.org/) installed on your computer

## Getting started

This tutorial will go through the whole procedure of:
- Creating a resource registry with SWITCH
- Configuring SWITCH as an IDP for your realm
- Configuring mappers for the basic attributes
- Testing the authentication with SWITCH with the keycloak internal client

This tutorial will **not** cover the following parts:
- Configuring your client (application)
- Testing your client (application)
- Configuring mappers for the extended attributes

## Opening all the requested tabs

We will need to constantly switch between the keycloak server, the realm metadata and the AAI Resource Registry.

To make things easier, we recommend to keep the three tabs open.

### Keycloak

Go to https://login.biomedit.ch or https://keycloak-test.dcc.sib.swiss depending on which
realm you are trying to configure. Connect using your keycloak account. Select your realm in
the dropdown on the left. Then go to "Realm settings". You should have something like this:

![5.png](images%2F5.png)

### Metadata

Click on "SAML 2.0 Identity Provider Metadata" to open a new tab with realm metadata.

### AAI Resource Registry

Go here https://rr.aai.switch.ch/ and connect using your SWITCH edu-ID account.

![1.png](images%2F1.png)

You should arrive at the landing page.

![2.png](images%2F2.png)

## Identity provider

An identity provider derives from a specific protocol used to authenticate and send authentication and authorization information to users. It can be:

- A social provider such as Facebook, Google, or Twitter.
- A business partner whose users need to access your services.
- A cloud-based identity service you want to integrate.

Typically, Keycloak bases identity providers on the following protocols:

- SAML v2.0
- OpenID Connect v1.0
- OAuth v2.0

### Configure a new Identity provider

Open your keycloak tab and click on "Identity providers" in the left menu. Then, 
click on "SAML v2.0".

You should have something like this

![8.png](images%2F8.png)

- Change the alias to use `switch_edu-id` instead of `saml`
- Change the Display name to `SWITCH edu-ID IdP`
- Uncheck "Use entity descriptor" checkbox
- Fill the "Single Sign-On service URL" field with `https://login.eduid.ch/idp/profile/SAML2/POST/SSO`
- Change "NameID policy format" to `Persistent`
- Check "HTTP-POST binding response" checkbox
- Check "HTTP-POST binding for AuthnRequest" checkbox
- Check "Want Assertions encrypted" checkbox
- Check "Force authentication" checkbox
- Check "Validate Signatures" checkbox
- Fill the "Validating X509 certificates" field with the following value `MIIDADCCAeigAwIBAgIJAKpNC+SfjVCyMA0GCSqGSIb3DQEBCwUAMBMxETAPBgNV BAMTCGVkdWlkLmNoMB4XDTIxMDEwNDE1NTkyNloXDTI0MDEwNTE1NTkyNlowEzER MA8GA1UEAxMIZWR1aWQuY2gwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB AQCbkUjiqUSI7m8LCNeIW/A3V+frMWd+DPvHzpEwIXLvzDPXts6lCGzI3Yo6lNkF 7VMUh6mxEXegu3t8AbaOsfrroFijIxMdQ4Pq3Z/Ps2ssa0KAxkcpHnWP011la1TN 7hsxq07Xsat++CHVescJ8zk7Qd+uZmmfW5Kfj5KQhOPoQBMULVtak/Kjeolo4Ur1 fwHtu8gvTvwrBVMy0wYMeylBmLL73dKYFO2mZLs5lP0FKXTbmAUPF+WfkOckuExY vetWxLDyiDaxs5j3/KqJsDSiPQq1oGZkqvPfC4NzfCUTsuW5RdPierusXpnR4Wd8 gj3feS4d6fGrt0arFUYkiyjFAgMBAAGjVzBVMDQGA1UdEQQtMCuCCGVkdWlkLmNo hh9odHRwczovL2VkdWlkLmNoL2lkcC9zaGliYm9sZXRoMB0GA1UdDgQWBBR5UYvm 41G9T4lRYQnVBamV5QQ2czANBgkqhkiG9w0BAQsFAAOCAQEAguD8iGdY9Umd/zBC o8Hv+HiDsqdXumMWdB3faFuA69IJhbnuVdC8pVOEa05XOrTon+TO5khk43UB/5bW JxHPmdGv9xzp+ePXovOUV3dWACbZyjXzjMehaaEM27cIHPCa5kDga4R6c4dycTSV aOUc34+Mazpa5gikRLJ6ApJIH0ytlJFwsqUZ949+op6iatdkB90XYggy540OmrNs 9rt/zZ2kx/qxf2w1FpcsiQQZLz8UPMkRml3naUosRKF/gqWtWYvhmvvOc/F32Qlc utz6C7DN2kXOFZg9EP0Rie1vZwCMfMFt53fsy/CukJE/Vt+MDdHHmQyMP1lZqh9U VC6NzA== `
- Fill "Allowed clock skew" with 10

You should end up with something like this:

![9.png](images%2F9.png)

Click on "Add" at the bottom.

You should now have a new section "Advanced settings" at the bottom.

- Check the "Trust Email" checkbox

Click on "Save" at the bottom and keep this tab open.

## Resource registry

The Resource Registry is a web-based tool developed by SWITCH to manage information
about Resources and Home Organizations participating in the SWITCHaai and AAI Test
federations, which are operated by SWITCH. Since 2011 the Resource Registry is also
capable of handling interfederated Resources and Home Organizations.

To put it simply, the Resource Registry is a tool allowing you to configure your
realm to use SWITCH edu-ID as an Identity provider.

### Create a new resources registry

Open your AAI Resource Registry tab and click on "Resources" at the top or go here: https://rr.aai.switch.ch/menu_res_options.php

Then, click on "Add a Resource Description".

![3.png](images%2F3.png)

Select then "SAML resource at the top".

![6.png](images%2F6.png)

### Basic Resource Information

Go back to the AAI Resource Registry and click on "Basic Resource Information".

**Basic Properties**

- Fill the Entity ID field with the value from the realm metadata tab under "entityID". For example, https://login.biomedit.ch/realms/swissgenvar
- Choose "Université de Lausanne (unil.ch, SWITCHaai)" as the Home Organization.
- Type the name of the project in the "Name" field. For example, "SwissGenVar"
- Fill the "Description" field with a description of the project (keep it short, <100 characters)
- Keep the "Public" unchecked unless you want your project to be listed in the list of public resources

**Home and Helpdesk URLs**

- Fill the "Home URL" field with either https://login.biomedit.ch or https://keycloak-test.dcc.sib.swiss/ depending on which one you use. 

**Additional Resource Administrators**

- Add any colleagues who should also be an administrator of this resource registry. For the moment, only @vbarbie, @gbouchet, @dwalther and @dterumal should be administrators.

Leave the remaining fields with their default values.

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

### Descriptive Information

Click on "Descriptive Information".

**Main language**

- Keep "English" as the "Main language"

You can leave the rest of the fields as they are unless you would like to provide translations of the project name and description in other languages.

You can also upload logos of your project if you desire.

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

### Contacts

Click on "Contacts".

Fill all the contacts (Support, Technical and Administrative) as follows:

- **Support contact**: it should be the project manager
- **Techincal contact**: it should be the devs/lead dev
- **Administrative contact**: it should be the group leader

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

### Service Locations

Click on "Service Locations".

Switch to your keycloak tab which should be open on the identity provider setup. If 
that's not the case, simply click on "Identity providers" on the left menu and click 
on "switch_edu-id" in the list. Then copy the "Redirect URI" value.

Go back to the back to your AAI Resource Registry tab which should be on the "Service 
Locations" settings. Paste the copied "Redirect URI" value in both fields:

- **Assertion Consumer Service**>SAML2 HTTP POST
- **Single Logout Service**>SAML2 HTTP POST

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

### Certificates and Credentials

Click on "Certificates and Credentials"

#### Generate a new certificate

Go here https://www.switch.ch/aai/guides/sp/embedded-certificate/.

- Fill "Fully Qualified Domain Name" with the value entityID from the realm metadata

You can leave the rest of fields with their default values.

Then, simply follow the instructions here (https://www.switch.ch/aai/guides/sp/embedded-certificate/).

When typing the command `openssl x509 -in sp-cert.pem -nameopt show_type,sep_comma_plus_space -text`,
you should have the certificate in the output, properly formatted for SWITCH. Copy everything between
`-----BEGIN CERTIFICATE-----` and `-----END CERTIFICATE-----` including the headers.

Switch to your AAI Resource Registry tab, and paste the copied field in the "Certificates" field

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

Switch to your keycloak tab and click on "Realm settings" on the left menu. Click on "Keys"
at the top, and you should have something like this:

![10.png](images%2F10.png)

Click on "Providers" and "Add provider". Select `rsa-enc` (THIS IS IMPORTANT) as the provider. Make sure that "Enabled" and "Active" are `On`.
and select the `sp-key.pem` for the "Private RSA Key" field, and the `sp-cert.pem` for the "X509 Certificate"
field. Finally, select `RSA-OAEP` for the "Algorithm".

![11.png](images%2F11.png)

Click on "Save" and go back to your "AAI Resource Registry" tab.

### Requested Attributes and Claims

Click on "Requested Attributes and Claims"

- In the "SWITCHaai Core Attributes" section, put everything on `Required`
- In the "Interfederation Core Attributes" section, put everything on `Desired`
- Leave all the other section as `-` (empty)

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

### Intended Audience and Expert Settings

Click on "Intended Audience and Expert Settings"

- Leave the "Interfederation Support and Entity Categories" completely unchecked
- Leave all the fields in the "Default Intended Audience" section as excluded
- In the "SWITCH edu-ID Private Identity", select included, which should open a confirmation dialog and click on "Okay"

You should have something similar to this:

![12.png](images%2F12.png)

Click on "Apply" at the bottom to check if you have any error. If not, click on "Save and continue".

### Submit for approval

Now you can submit your resource description for approval. You can fill the comment field with some explanations on why
you added this Resource Description or what you changed.

Then click on "Submit for Approval".

The approval process can take a few days before activation and inclusion in the official federation metadata.

## Configuring mappers for the basic attributes

A basic user in keycloak possess some basic attributes such as email, username, first name and last name. To force 
our realm to extract those attributes from the IdP, we need to add a few mappers.

### Adding the email mapper

Go to https://login.biomedit.ch or https://keycloak-test.dcc.sib.swiss depending on which
realm you are trying to configure. Connect using your keycloak account. Select your realm in
the dropdown on the left.

Click on "Identity providers" in the left menu and select the "Mappers" tab. Click on "Add mapper".

Fill the mapper as such:

- **Name**: `email`
- **Sync mode override**: `Inherit`
- **Mapper type**: `Attribute Importer`
- **Attribute Name**: `urn:oid:0.9.2342.19200300.100.1.3`
- **Name Format**: `ATTRIBUTE_FORMAT_BASIC`
- **User Attribute Name**: `email`

You should have this:

![13.png](images%2F13.png)

Click on "Save" and go back to the mappers list.

### Adding the last name mapper

Click on "Add mapper". Fill the mapper as such:

- **Name**: `lastName`
- **Sync mode override**: `Inherit`
- **Mapper type**: `Attribute Importer`
- **Attribute Name**: `urn:oid:2.5.4.4`
- **Name Format**: `ATTRIBUTE_FORMAT_BASIC`
- **User Attribute Name**: `lastName`

You should have this:

![14.png](images%2F14.png)

Click on "Save" and go back to the mappers list.

### Adding the last name mapper

Click on "Add mapper". Fill the mapper as such:

- **Name**: `firstName`
- **Sync mode override**: `Inherit`
- **Mapper type**: `Attribute Importer`
- **Attribute Name**: `urn:oid:2.5.4.42`
- **Name Format**: `ATTRIBUTE_FORMAT_BASIC`
- **User Attribute Name**: `firstName`

You should have this:

![15.png](images%2F15.png)

Click on "Save" and go back to the mappers list.

### Adding the Swiss Edu Person Unique ID

This attribute is required as it is unique per identity/person, therefore we will use it as the username.

Click on "Add mapper". Fill the mapper as such:

- **Name**: `swissEduPersonUniqueID`
- **Sync mode override**: `Inherit`
- **Mapper type**: `Username Template Importer`
- **Template**: `${ATTRIBUTE.swissEduPersonUniqueID}`
- **Target**: `LOCAL`

You should have this:

![16.png](images%2F16.png)

Click on "Save" and go back to the mappers list.

If you would like to use other mappers, you can find the list here: https://www.switch.ch/aai/support/documents/attributes/

## Testing the authentication with a SWITCH edu-ID account

*Before you can proceed with this step, your resource description must have been approved*

Go to https://login.biomedit.ch or https://keycloak-test.dcc.sib.swiss depending on which
realm you are trying to configure. Connect using your keycloak account. Select your realm in
the dropdown on the left.

Click on "Clients" in the left menu and then click on the "Home URL" of the row with the "Client
ID" `account`. Then, click on "Sign in" on the top right and sign in with "SWITCH edu-ID IdP". If
everthing is properly configured, you should be able to connect without any error. If you encounter
any issue, feel free to contact Gerard.Bouchet@sib.swiss or dillenn.terumalai@sib.swiss.
